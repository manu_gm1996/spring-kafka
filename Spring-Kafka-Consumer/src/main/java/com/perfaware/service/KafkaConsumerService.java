package com.perfaware.service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerService {

	public ResponseEntity<String> consumer(String topicName, int messages) {

		Properties properties=new Properties();
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "sandbox-hdf.hortonworks.com:6667");
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, String.valueOf(messages));
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "group_one");

		KafkaConsumer<String, String> consumer=new KafkaConsumer<String, String>(properties);

		ConsumerRecords<String, String> consumer_records = new ConsumerRecords<>(null);

		consumer.subscribe(Arrays.asList(topicName));

		List<String> newlist = new ArrayList<String>();
		
		consumer_records = consumer.poll(100);
		
		for(ConsumerRecord<String, String> record:consumer_records)
		{
			System.out.println(record.value());
			newlist.add(record.value());
		}
		System.out.println(newlist);
		consumer.close();
		return ResponseEntity.status(200).body(newlist.toString());
	}
}