package com.perfaware.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.perfaware.service.KafkaConsumerService;

@Controller
public class KafkaConsumerController {

	private KafkaConsumerService kafkaConsumerService;

	private KafkaConsumerController(KafkaConsumerService kafkaConsumerService) {
		this.kafkaConsumerService = kafkaConsumerService;
	}

	@GetMapping("springkafka/{topicName}/{message}")
	@ResponseBody
	public ResponseEntity<String> Controller(@PathVariable("topicName") String topicName,
			@PathVariable("message") int messages) {
		return kafkaConsumerService.consumer(topicName, messages);
	}
}
